<?php

/*
amf_Test:getData.php
March 10, 2015
Michael Bourque

This routine produces JSON encoded represenation of the table tNative

*/


// Force any HTML output to UTF-8 Encoding.
header('Content-Type: text/html; charset=utf-8');

//Connect to the database for Reading
require("connect.php");
//require("dataObjectClass.php");

//Create and Execute SELECT query
  $query = "SELECT * from tNative";

//MySqli Select Query
$q = $datacon->query($query);
$rows = array();

while($row = $q->fetchObject()) {
	$rows[] = $row;
}
$json=(json_encode($rows));
print_r($json);

// close connection 
//$mysqli->close();


?>
