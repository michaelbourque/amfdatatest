<?php

/*
amf_Test:connect.php
March 9, 2015
Michael Bourque

Database Connection Abstraction

*/


// Load Global Variables
require_once('config.php');

try {
    $datacon = new PDO('mysql:dbname='.$_DATABASE_NAME.';host='.$_DATABASE_HOST, $_DATABASE_USER, $_DATABASE_PASS, array(
    PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
));
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

?>
