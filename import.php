<?php
/*
amf_Test:import.php
March 9, 2015
Michael Bourque
-- Import contents of Microsoft Excel Spreadsheet to MySQL Database

** Table Structure

-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 10, 2015 at 03:00 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*
--
-- Database: `amf_Test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tNative`
--

CREATE TABLE IF NOT EXISTS `tNative` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'AI Indexed ID',
  `status` varchar(8) NOT NULL DEFAULT 'active' COMMENT 'Status of Risk',
  `risk` varchar(64) NOT NULL COMMENT 'Description of Risk (Business Type)',
  `language` char(1) NOT NULL COMMENT 'Language (English or French)',
  `liability` text NOT NULL COMMENT 'Applications for Liability',
  `property` text NOT NULL COMMENT 'Applications for Property',
  `eando` text NOT NULL COMMENT 'Applications for Errors and Omissions',
  `excess` text NOT NULL COMMENT 'Applications for Excess',
  `umbrella` text NOT NULL COMMENT 'Applications for Umbrella',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=940 ;

*/


// Force any HTML output to UTF-8 Encoding. (only necessary if displaying data directly from spreadsheet)
header('Content-Type: text/html; charset=utf-8');

// Connect to database
require('connect.php');
require('functions.php');

// Select how many iniital rows to skip
$skipRows = 1;

// Enable Error Reporting for Debugging 
if ( $runMode==1 ) {
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);
  date_default_timezone_set('Europe/London'); // Required by PHPExcel - Using Defaults
  define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
}

// Include PHPExcel (For this pupose we only need PHPExcel_IOFactory but including whole library for development
require_once 'Classes/PHPExcel.php';

// Input filename for XLS Data
$inputFileName = "./data/programmertest2015.xlsx";

//  Identify the type of $inputFileName (We may not always know for sure)
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);


//  Create a new Reader of the type defined in $inputFileType
try {
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
} catch(Exception $e) {
      die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

$objReader->setReadDataOnly(true); //Set Open Data for ReadOnly (No Need to Change Data)
$objPHPExcel = $objReader->load($inputFileName); //Load Data from Reader into the Excel Object
$objWorksheet = $objPHPExcel->setActiveSheetIndex(0); //Making Assumption that Data is ALWAYS the first sheet

// Determine the highest Row in the Spread Sheet
$highestRow = $objWorksheet->getHighestRow();

echo "Importing " . $highestRow . " Row(s). Skipping " . $skipRows . " Row(s). <br />";

$skipCount = 0;
// Read rows 1 by 1
for ($row = 0; $row <= $highestRow; ++$row) {

    //Retrieve Cell Data (with proper formatting)
    $status = $objWorksheet->getCellByColumnAndRow(1, $row)->getFormattedValue();
    $risk = $objWorksheet->getCellByColumnAndRow(2, $row)->getFormattedValue();
    $language = $objWorksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
    $liability = fileExplodeHREF($objWorksheet->getCellByColumnAndRow(4, $row)->getFormattedValue());
    $property = fileExplodeHREF($objWorksheet->getCellByColumnAndRow(5, $row)->getFormattedValue());
    $eando = fileExplodeHREF($objWorksheet->getCellByColumnAndRow(6, $row)->getFormattedValue());
    $excess = fileExplodeHREF($objWorksheet->getCellByColumnAndRow(7, $row)->getFormattedValue());
    $umbrella = fileExplodeHREF($objWorksheet->getCellByColumnAndRow(8, $row)->getFormattedValue());
 
 
    //Create and Execute INSERT query
    $fields = "status, risk, language, liability, property, eando, excess, umbrella";
    $values = ":status,:risk,:language,:liability,:property,:eando,:excess,:umbrella";
    $query = "INSERT INTO tNative (" . $fields . ") VALUES(" . $values . ")";

    $q = $datacon->prepare($query);
  
    if ($skipCount > $skipRows) {
        $q->execute(array(':status'=>$status, ':risk'=>$risk, ':language'=>$language, ':liability'=>$liability, ':property'=>$property, ':eando'=>$eando, ':excess'=>$excess, ':umbrella'=>$umbrella));
    }

        $skipCount++;
}
// close connection
$import->close();

echo "Done.";
?>
