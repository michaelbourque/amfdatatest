This is a very basic application that displays a list of PDF documents and allows searching in French or English.

There is an import.php that creates a database, and parses a Spreadsheet to stuff records into MySQL

The UI uses PHP, Bootstrap, and AngularJS.

This project was a learning experiment to familiarize myself with the infrastructures and technologies of a new company as well as to explore the functionality of Angular.